import {
  ADD_TO_CART,
  REMOVE_FROM_CART,
  UPDATE_CART_ITEM,
} from "../actionTypes/cart";

const initialState = {};

export default function productReducer(state = initialState, action) {
  switch (action.type) {
    case UPDATE_CART_ITEM:
      let new_qty = state[action.payload.id].qty + action.payload.qty;
      if(new_qty < 1) { new_qty = 1 }

      return {
        ...state,
        [action.payload.id]: {
          ...state[action.payload.id],
          qty: new_qty,
        },
      };

    case REMOVE_FROM_CART:
      const itemId = action.payload.id;
      let new_state = Object.assign({}, state);
      delete new_state[itemId];
      return new_state;

    case ADD_TO_CART:
      let qty = 0;
      if (state[action.payload.id]) {
        qty = action.payload.qty + state[action.payload.id].qty;
      } else {
        qty = action.payload.qty;
      }
      return {
        ...state,
        [action.payload.id]: { ...action.payload, qty: qty },
      };
    default:
      return state;
  }
}
