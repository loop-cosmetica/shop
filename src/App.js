import React, { useState } from "react";
import ProductList from "./components/products/productList";
import NavBar from "./components/NavBar";
import AppBody from "./components/AppBody";
import CartPanel from "./components/CartPanel";
import Modal from "./components/Modal";

import "./App.css";

const App = () => {
  const [showCartPanel, setCartPanel] = useState(false);

  const closeCartPanel = () => setCartPanel(false);
  const openCartPanel = () => setCartPanel(true);

  return (
    <div className="shopping-list">
      <NavBar openCartPanel={openCartPanel} />
      <AppBody>
        <Modal show={showCartPanel}>
          <CartPanel close={closeCartPanel} />
        </Modal>
        <ProductList />
      </AppBody>
    </div>
  );
};

export default App;
