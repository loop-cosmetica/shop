import {
  ADD_TO_CART,
  REMOVE_FROM_CART,
  UPDATE_CART_ITEM,
} from "../actionTypes/cart";

export const addToCart = (product) => ({
  type: ADD_TO_CART,
  payload: {
    id: product.id,
    name: product.name,
    cents: product.cents,
    image_url: product.image_url,
    qty: product.qty,
  },
});

export const removeFromCart = (id) => ({
  type: REMOVE_FROM_CART,
  payload: { id: id },
});

export const updateCartItem = (id, qty) => ({
  type: UPDATE_CART_ITEM,
  payload: { id: id, qty: qty },
});
