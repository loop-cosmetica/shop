import React from "react";

import "./index.css";

const LinkButton = (props) => {
  const {
    history,
    location,
    match,
    staticContext,
    to,
    onClick,
    // ⬆ filtering out props that `button` doesn’t know what to do with.
    ...rest
  } = props
  return (
    <button className="link-button"
      {...rest} // `children` is just another prop!
      onClick={(event) => {
        onClick && onClick(event)
      }}
    />
  )
}

export default LinkButton;
