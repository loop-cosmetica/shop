import React, { useEffect } from "react";

import "./index.css";

export const useDisableBodyScroll = (open) => {
  useEffect(() => {
    if (open) {
      document.body.style.overflow = 'hidden';
    } else {
      document.body.style.overflow = 'unset';
    }
  }, [open]);
};

const Modal = (props) => {
  const { show, ...rest } = props;
  useDisableBodyScroll(show);

  if (show) {
    return <div className="modal" {...rest} />;
  } else {
    return null;
  }
};

export default Modal;
