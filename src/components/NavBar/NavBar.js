import React from "react";
import LinkButton from "../../components/LinkButton"
import mainLogo from "../../logo.png";
import shoppingCartIcon from "../../assets/images/shopping-cart.svg";

import "./index.css";


const NavBar = ({openCartPanel, totalQty}) => {
  return (
    <nav>
      <div className="container">
        <img src={mainLogo} alt="Loop logo" />
        <div className="action-buttons">
          <LinkButton onClick={openCartPanel}>
          <img onClick={openCartPanel} src={shoppingCartIcon} alt="shopping cart icon" />
          <span className="total-qty">{totalQty}</span>
          </LinkButton>
        </div>
      </div>
    </nav>
  );
};

export default NavBar;
