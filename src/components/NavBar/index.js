import NavBar from "./NavBar";
import { connect } from "react-redux";
import {totalQty} from "../../selectors"

const mapStateToProps = (state) => ({
  cart: state.cart,
  totalQty: totalQty(state.cart)
});

export default connect(mapStateToProps)(NavBar);
