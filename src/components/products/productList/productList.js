import React from "react";
import "./index.css";
import ProductItem from "../productItem";

class ProductList extends React.Component {
  componentDidMount() {
    this.props.fetchProducts();
  }

  render() {
    const productItems = this.props.products.records.map(({ id, name, image_url, cents }) => (
      <ProductItem
        key={id}
        name={name}
        id={id}
        cents={cents}
        image_url={image_url}
      />
    ));

    return (
      <div id="product--list">
        <h1>Lista de productos</h1>
        <ul>{productItems}</ul>
      </div>
    );
  }
}

export default ProductList;
