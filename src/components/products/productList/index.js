import ProductList from "./productList";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchProducts } from "../../../actions/products";

const mapStateToProps = (state) => ({
  products: state.products,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      fetchProducts: fetchProducts,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ProductList);
