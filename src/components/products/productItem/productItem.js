import React, { useState } from "react";
import { useAlert } from 'react-alert'

import LinkButton from "../../LinkButton"

import "./index.css";

const ProductItem = ({ id, name, image_url, cents, addToCart }) => {
  const [qty, setQty] = useState(1);
  const alert = useAlert()

  const handleAddTodo = () => {
    addToCart({ id, name, cents, qty, image_url });
    alert.success("Agregaste "+qty + " "+name +" al carrito")
  };

  const increaseQty = () => setQty(qty + 1);
  const decreaseQty = () => (qty > 1 ? setQty(qty - 1) : null);

  return (
    <li className="product--item">
      <img alt={name} src={image_url} />
      <h2>{name}</h2>
      <p className="price">$ {cents}</p>

      <p>
        cantidad: <LinkButton onClick={decreaseQty}>
          &lt;{" "}
        </LinkButton>
        {qty}
        <LinkButton onClick={increaseQty}>
          >
        </LinkButton>
      </p>
      <button className="btn-dark" onClick={handleAddTodo}>Agregar al carrito</button>
    </li>
  );
};

export default ProductItem;
