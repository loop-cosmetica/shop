import ProductItem from "./productItem";
import { connect } from 'react-redux'
import { bindActionCreators } from "redux";
import { addToCart } from '../../../actions/cart'

import "./index.css";

const mapStateToProps = (state) => ({
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      addToCart: addToCart
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ProductItem);
