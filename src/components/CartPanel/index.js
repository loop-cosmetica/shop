import CartPanel from "./CartPanel";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getTotal, totalQty, whatsappCheckout } from "../../selectors"

const mapStateToProps = (state) => ({
  cart: state.cart,
  totalQty: totalQty(state.cart),
  getTotal: getTotal(state.cart),
  whatsappCheckout: whatsappCheckout(state.cart)
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      // fetchProducts: fetchProducts,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(CartPanel);
