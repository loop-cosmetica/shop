import React from "react";
import CartList from "../../components/cart/cartList";
import LinkButton from "../../components/LinkButton";

import "./index.css";

const CartPanel = (props) => (
  <div id="cart-panel" className="modal-container">
    <LinkButton className="close" onClick={props.close}>
      X
    </LinkButton>
    <div className="modal-header">
      <h1>Carrito ({props.totalQty})</h1>
    </div>
    <CartList />
    <div className="modal-footer">
      <div className="subtotal">
        <strong>Total</strong>
        <strong>$ {props.getTotal}</strong>
      </div>
      <div className="checkout">
        <a className="btn btn-block btn-dark" href={props.whatsappCheckout}>
          Hacer pedido
        </a>
      </div>
    </div>
  </div>
);

export default CartPanel;
