import React from "react";

import "./index.css";

const AppBody = (props) => {
  const {
    ...rest
  } = props
  return (
    <div className="app-body"
      {...rest}
    />
  )
}

export default AppBody;
