import CartList from "./cartList";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

const mapStateToProps = (state) => ({
  cart: state.cart,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      // fetchProducts: fetchProducts,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(CartList);
