import React from "react";
import CartItem from "../cartItem"
import "./index.css";

class CartList extends React.Component {
  render() {
    const items = this.props.cart;
    return (
      <ul id="cart-list">
        {Object.keys(items).map((item, i) => (
          <CartItem item={items[item]}/>
        ))}
      </ul>
    );
  }
}

export default CartList;
