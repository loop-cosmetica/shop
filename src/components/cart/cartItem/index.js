import CartItem from "./cartItem";
import { connect } from 'react-redux'
import { bindActionCreators } from "redux";
import { removeFromCart, updateCartItem } from '../../../actions/cart'

import "./index.css";

const mapStateToProps = (state) => ({
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      removeFromCart: removeFromCart,
      updateCartItem: updateCartItem
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(CartItem);
