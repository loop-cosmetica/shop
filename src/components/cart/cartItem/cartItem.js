import React from "react";
import LinkButton from "../../LinkButton";

const CartItem = ({ item, removeFromCart, updateCartItem }) => {
  const handleRemoveItem = () => removeFromCart(item.id);
  const increaseQty = () => updateCartItem(item.id, 1);
  const decreaseQty = () => updateCartItem(item.id, -1);

  return (
    <li className="cart-item">
      <div className="product-img">
        <img alt={item.name} src={item.image_url} />
      </div>
      <div className="product-detail">
        <h1>{item.name}</h1>
        <span className="text-muted">${item.cents}</span>
        <div className="actions">
          <LinkButton onClick={decreaseQty}>&lt; </LinkButton> {item.qty} <LinkButton onClick={increaseQty}> ></LinkButton>
          <div className="remove">
            <LinkButton onClick={handleRemoveItem}>x Quitar</LinkButton>
          </div>
        </div>
      </div>
    </li>
  );
};

export default CartItem;
