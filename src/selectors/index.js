export const totalQty = (items) => {
  let total = 0;
  Object.keys(items).map(
    (item) => (total += items[item].qty)
  );

  return total;
};

export const getTotal = (items) => {
  let total = 0;
  Object.keys(items).map(
    (item) => (total += items[item].qty * items[item].cents)
  );

  return total;
};

export const whatsappCheckout = (items) => {
  var message = "Hola, quiero comprar: "
  Object.keys(items).map(
    (item) => (message += items[item].qty + " " + items[item].name + ", ")
  );
  message += "Por un total de $" + getTotal(items)

  return(
    'https://wa.me/5493425785489/?text='+ message.replace(/&/g, '%26')
  )
}
